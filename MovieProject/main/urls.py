from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^action', views.action, name='action'),
    url(r'^adventure', views.adventure, name='adventure'),
    url(r'^animation', views.animation, name='animation'),
    url(r'^comedy', views.documentary, name='comedy'),
    url(r'^documentary', views.documentary, name='documentary'),
    url(r'^family', views.family, name='family'),
    url(r'^fantasy', views.fantasy, name='fantasy'),
    url(r'^musical', views.musical, name='musical'),
    url(r'^romance', views.romance, name='romance'),
    url(r'^sciencefiction', views.sciencefiction, name='sciencefiction'),
    url(r'^thriller', views.thriller, name='thriller'),


]
