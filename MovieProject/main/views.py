from django.shortcuts import render
from django.http import HttpResponse
import pymysql
import MySQLdb

def getSearch(request):
    if request.method == 'GET':
        search = request.GET.get('searchField', None)
        typeOfResult = request.GET.get('typeOfResult', None)
        nbOfResult = request.GET.get('nbOfResult', None)
    return {'search' : search, 'typeOfResult' : typeOfResult, 'nbOfResult' : nbOfResult}

def encodeQuery(rowBis):                                                                               
    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str
    
    jstr = '''{"films":['''


    if len(rowBis)>=1:
        for i in rowBis:
            jstr += '''{"img":"''' + "" + '''",'''
            jstr += '''"title":"''' + str(i[0]) + '''"},'''
            jstr += '''"score":"''' + str(i[1]) + '''"},'''
            jstr += '''"author":"''' + str(i[2]) + '''"},'''
            jstr += '''"year":"''' + str(i[3]) + '''"},'''
            jstr += '''"desc":"''' + str(i[4]) + '''"},'''
            jstr += '''"more":"''' + str(i[5]) + '''"},'''
            jstr += '''"categorie":["''' 
	    
            for j in rowBis[6]:
                jstr += '''"{"label":"''' + str(i[6][j]) + '''"},'''
            
            jstr = jstr[:-1]
            jstr += ''']'''
        jstr = jstr[:-1]

    jstr+= ']}'

    return jstr

def generateQuery(s):
    ## TODO GENERATE QUERY ie -> select xx from **...
    ## s is a dictionnary and contains the "search", "nbOfResult", typeOfresult	
    nbOfResult = s.get('nbOfResult')
    search = s.get('search')
    typeOfResult = s.get('typeOfResult')
    result = "SELECT * FROM table WHERE movie_title LIKE " + search + ""
    result += "ORDER BY " + "typeOfResult"
    result += "LIMIT" + nbOfResult + ";"
    print(result)
    return result

def index(request):
    category = 'All'
    
    s = getSearch(request)
    result = generateQuery(s, category)

    rowBis = []
    conn = pymysql.connect(host='127.0.0.1', user='root', passwd='root', db='movie')
    cur = conn.cursor()
    cur.execute(result)
    for response in cur:
        rowBis.append(response)
    cur.close()
    conn.close()


    json = encodeQuery(rowBis)

    tOr = s.get('typeOfResult')
    nOr = s.get('nbOfResult')

    return render(request, 'src/index.html',
            {'category' : category, 'typeOfResult' : tOr, 'nbOfResult' : nOr, 
		'json': json}
            )

def action(request):
    category = 'Action'

    s = getSearch(request)
    result = generateQuery(s, category)
    rowBis = []
    conn = pymysql.connect(host='127.0.0.1', user='root', passwd='root', db='movie')
    cur = conn.cursor()
    cur.execute(result)
    for response in cur:
        rowBis.append(response)
    cur.close()
    conn.close()
    json = encodeQuery(result)

    tOr = s.get('typeOfResult')
    nOr = s.get('nbOfResult')

    return render(request, 'src/index.html',
            {'category' : category, 'typeOfResult' : tOr, 'nbOfResult' : nOr,'json': json}
            )

def adventure(request):
    category = 'Adventure'

    s = getSearch(request)
    result = generateQuery(s, category)
    rowBis = []
    conn = pymysql.connect(host='127.0.0.1', user='root', passwd='root', db='movie')
    cur = conn.cursor()
    cur.execute(result)
    for response in cur:
        rowBis.append(response)
    cur.close()
    conn.close()
    json = encodeQuery(result)

    tOr = s.get('typeOfResult')
    nOr = s.get('nbOfResult')

    return render(request, 'src/index.html',
            {'category' : category, 'typeOfResult' : tOr, 'nbOfResult' : nOr,'json': json}
            )

def animation(request):
    category = 'Animation'

    s = getSearch(request)
    result = generateQuery(s, category)
    rowBis = []
    conn = pymysql.connect(host='127.0.0.1', user='root', passwd='root', db='movie')
    cur = conn.cursor()
    cur.execute(result)
    for response in cur:
        rowBis.append(response)
    cur.close()
    conn.close()
    json = encodeQuery(result)

    tOr = s.get('typeOfResult')
    nOr = s.get('nbOfResult')

    return render(request, 'src/index.html',
            {'category' : category, 'typeOfResult' : tOr, 'nbOfResult' : nOr,'json': json}
            )

def comedy(request):
    category = 'Comedy'

    s = getSearch(request)
    result = generateQuery(s, category)
    rowBis = []
    conn = pymysql.connect(host='127.0.0.1', user='root', passwd='root', db='movie')
    cur = conn.cursor()
    cur.execute(result)
    for response in cur:
        rowBis.append(response)
    cur.close()
    conn.close()
    json = encodeQuery(result)

    tOr = s.get('typeOfResult')
    nOr = s.get('nbOfResult')

    return render(request, 'src/index.html',
            {'category' : category, 'typeOfResult' : tOr, 'nbOfResult' : nOr,'json': json}
            )

def documentary(request):
    category = 'Documentary'

    s = getSearch(request)
    result = generateQuery(s, category)
    rowBis = []
    conn = pymysql.connect(host='127.0.0.1', user='root', passwd='root', db='movie')
    cur = conn.cursor()
    cur.execute(result)
    for response in cur:
        rowBis.append(response)
    cur.close()
    conn.close()
    json = encodeQuery(result)

    tOr = s.get('typeOfResult')
    nOr = s.get('nbOfResult')

    return render(request, 'src/index.html',
            {'category' : category, 'typeOfResult' : tOr, 'nbOfResult' : nOr,'json': json}
            )

def family(request):
    category = 'Family'

    s = getSearch(request)
    result = generateQuery(s, category)
    rowBis = []
    conn = pymysql.connect(host='127.0.0.1', user='root', passwd='root', db='movie')
    cur = conn.cursor()
    cur.execute(result)
    for response in cur:
        rowBis.append(response)
    cur.close()
    conn.close()
    json = encodeQuery(result)

    tOr = s.get('typeOfResult')
    nOr = s.get('nbOfResult')

    return render(request, 'src/index.html',
            {'category' : category, 'typeOfResult' : tOr, 'nbOfResult' : nOr,'json': json}
            )

def fantasy(request):
    category = 'Fantasy'

    s = getSearch(request)
    result = generateQuery(s, category)
    rowBis = []
    conn = pymysql.connect(host='127.0.0.1', user='root', passwd='root', db='movie')
    cur = conn.cursor()
    cur.execute(result)
    for response in cur:
        rowBis.append(response)
    cur.close()
    conn.close()
    json = encodeQuery(result)

    tOr = s.get('typeOfResult')
    nOr = s.get('nbOfResult')

    return render(request, 'src/index.html',
            {'category' : category, 'typeOfResult' : tOr, 'nbOfResult' : nOr,'json': json}
            )

def musical(request):
    category = 'Musical'

    s = getSearch(request)
    result = generateQuery(s, category)
    rowBis = []
    conn = pymysql.connect(host='127.0.0.1', user='root', passwd='root', db='movie')
    cur = conn.cursor()
    cur.execute(result)
    for response in cur:
        rowBis.append(response)
    cur.close()
    conn.close()
    json = encodeQuery(result)

    tOr = s.get('typeOfResult')
    nOr = s.get('nbOfResult')

    return render(request, 'src/index.html',
            {'category' : category, 'typeOfResult' : tOr, 'nbOfResult' : nOr,'json': json}
            )

def romance(request):
    category = 'Romance'

    s = getSearch(request)
    result = generateQuery(s, category)
    rowBis = []
    conn = pymysql.connect(host='127.0.0.1', user='root', passwd='root', db='movie')
    cur = conn.cursor()
    cur.execute(result)
    for response in cur:
        rowBis.append(response)
    cur.close()
    conn.close()
    json = encodeQuery(result)

    tOr = s.get('typeOfResult')
    nOr = s.get('nbOfResult')

    return render(request, 'src/index.html',
            {'category' : category, 'typeOfResult' : tOr, 'nbOfResult' : nOr,'json': json}
            )

def sciencefiction(request):
    category = 'Sciencefiction'

    s = getSearch(request)
    result = generateQuery(s, category)
    rowBis = []
    conn = pymysql.connect(host='127.0.0.1', user='root', passwd='root', db='movie')
    cur = conn.cursor()
    cur.execute(result)
    for response in cur:
        rowBis.append(response)
    cur.close()
    conn.close()
    json = encodeQuery(result)

    tOr = s.get('typeOfResult')
    nOr = s.get('nbOfResult')

    return render(request, 'src/index.html',
            {'category' : category, 'typeOfResult' : tOr, 'nbOfResult' : nOr,'json': json}
            )

def thriller(request):
    category = 'Thriller'

    s = getSearch(request)
    result = generateQuery(s, category)
    rowBis = []
    conn = pymysql.connect(host='127.0.0.1', user='root', passwd='root', db='movie')
    cur = conn.cursor()
    cur.execute(result)
    for response in cur:
        rowBis.append(response)
    cur.close()
    conn.close()
    json = encodeQuery(result)

    tOr = s.get('typeOfResult')
    nOr = s.get('nbOfResult')

    return render(request, 'src/index.html',
            {'category' : category, 'typeOfResult' : tOr, 'nbOfResult' : nOr,'json': json}
            )
